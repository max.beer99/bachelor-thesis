import numpy as np
import os
import sys
import time

index = int(sys.argv[1])
root_path = './output/'
data_file_ending = '.dat'
figsize = (12,8)
only_plot_every_nth = 1
output = f'output/{time.strftime("%Y-%m-%d_%H-%M-%S")}_Combined_Time_Res'

def select_lines():
    file_list = os.listdir('{}.'.format(root_path))
    for i in np.arange(len(file_list))[::-1]:
    	if '.' in file_list[i]:
    		file_list.pop(i)

    for i in range(len(file_list)):
    	print('{}:\t{}'.format(i, file_list[i]))

    user_input = input('select file: ')

    user_input = int((len(file_list) - 1) if (user_input == '') else user_input)

    data_file_name = ''
    for filename in os.listdir('{0}{1}'.format(root_path, file_list[user_input])):
    	if data_file_ending in filename:
    		data_file_name = filename
    		break

    data_folder = file_list[user_input]
    print('selected: {}'.format('{0}{1}/{2}'.format(root_path, data_folder,data_file_name)))
    file = open('{0}{1}/{2}'.format(root_path, data_folder,data_file_name), 'r')
    lines = file.readlines()
    file.close()

    return lines, data_folder
    
lines_all = []
data_names = []
for i in range(index):
    lines, data_folder = select_lines()
    lines_all.append(lines)
    data_names.append(data_folder)

prefix = lines_all[0][:3:]
temp = []   
for i in range(len(lines_all)):
    for j in range(len(lines_all[i])):
        if not '#' in lines_all[i][j]:
            temp.append(lines_all[i][j])

lines = temp

index_jumps = []
line_length = 1
for i in range(1, len(lines)):
    line_length_0 = len(lines[i-1].replace('\n', '').split('\t'))
    line_length_1 = len(lines[i].replace('\n', '').split('\t'))
    line_length = line_length_0 if line_length_0 > line_length else line_length
    assert line_length_0 >= 6
    assert lines[i].replace('\n', '').split('\t')[line_length_1 - 1] >= lines[i-1].replace('\n', '').split('\t')[line_length_0 - 1]
    if lines[i].replace('\n', '').split('\t')[line_length_1 - 1] > lines[i-1].replace('\n', '').split('\t')[line_length_0 - 1]:
        index_jumps.append(i)

time_offsets = [0]
for index in index_jumps:
    time_offsets.append(float(lines[index-1].replace('\n', '').split('\t')[0]))

index_jumps = [0] + index_jumps
index_jumps.append(len(lines))

for i in range(1, len(time_offsets)):
    time_offsets[i] = time_offsets[i] + time_offsets[i-1] 

print(time_offsets)
print(index_jumps)

for i in range(len(time_offsets)):
    for j in range(index_jumps[i], index_jumps[i+1]):
        line = lines[j].split('\t')
        line[0] = str(float(line[0]) + time_offsets[i])
        lines[j] = f'{line[0]}' 
        for k in range(1, line_length):
            lines[j] += f'\t{line[k]}'
        
lines = prefix + lines
    
try:
    os.mkdir(output)
except:
    pass
    
file = open(f'{output}/data.dat', 'w+')
for line in lines:
    file.write(line)
file.close()

file = open(f'{output}/sources.txt', 'w+')
for name in data_names:
    file.write(f'{name}\n')
file.close()