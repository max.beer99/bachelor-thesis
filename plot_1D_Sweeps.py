# -*- coding: utf-8 -*-
"""
Created on Mon May 25 15:26:19 2020

@author: Max
"""

import numpy as np
import matplotlib.pyplot as plt
import matplotlib.colors as colors
import matplotlib.ticker as ticker
import os
import sys

#root_path = 'C:/Users/lab2/data/SiGe/HBT/Device1/2020-06-03/'
#root_path = '../06_Analysis/2020-06-04/Refined_Data/'
root_path = '../04_Data/2020-06-09/'
root_path = root_path if len(sys.argv) == 1 else sys.argv[1]
singled = False
singled = singled if len(sys.argv) <= 2 else True
data_file_ending = '.dat'
figsize = (12,8)
only_plot_every_nth = 1

def select_data():
      file_list = sorted(os.listdir('{}.'.format(root_path)))
      for i in np.arange(len(file_list))[::-1]:
      	if '.' in file_list[i]:
      		file_list.pop(i)

      for i in range(len(file_list)):
      	print('{}:\t{}'.format(i, file_list[i]))

      user_input = input('select file: ')

      user_input = int((len(file_list) - 1) if (user_input == '') else user_input)

      data_file_name = ''
      for filename in os.listdir('{0}{1}'.format(root_path, file_list[user_input])):
      	if data_file_ending in filename:
      		data_file_name = filename
      		break

      data_folder = file_list[user_input]
      print('selected: {}'.format('{0}{1}/{2}'.format(root_path, data_folder,data_file_name)))
      file = open('{0}{1}/{2}'.format(root_path, data_folder,data_file_name), 'r')
      lines = file.readlines()
      file.close()

      for i in range(len(lines)):
            lines[i] = lines[i].replace('\n','').replace('# ', '').replace('\"', '').replace('nan', '0')

      n_empty_lines = 1
      for i in np.arange(len(lines))[::-1]:
            if lines[i] == '':
                  lines.pop(i)
                  n_empty_lines += 1

      names = lines[0].split('\t')
      data = np.zeros((len(lines) - 3, len(lines[3].split('\t'))))

      for i in range(data.shape[0]):
            for j in range(data.shape[1]):
                  data[i, j] = lines[i+3].split('\t')[j]

      return data, names, data_folder, n_empty_lines

#%% 1D Sweeps
data, names, data_folder, n_empty_lines = select_data()

data[:, 0] = - data[:, 0]
data[:, 1] = - data[:, 1]

fig, ax = plt.subplots(ncols = 3, figsize = figsize)

ax[0].scatter(data[:, 0]*1e03, data[:, 1]*1e09 , marker = '.', label = names[1])
ax[0].scatter(data[:, 0]*1e03, data[:, 3]*1e09 , marker = '.', label = names[3])

minimum = min([min(data[:, 1]), min(data[:, 3])])*1e09
maximum = max(data[:, 3])*1e09
ax[0].set_ylim(minimum - 0.05*(maximum-minimum), maximum + 0.05*(maximum-minimum))
ax[0].set(xlabel = r'$V$ $[mV]$', ylabel = r'$I$ $[nA]$')

ax[1].scatter(data[:, 3]*1e09, data[:, 1]*1e09 , marker = '.', label = names[3])

minimum = min(data[:, 3])*1e09
maximum = max(data[:, 3])*1e09
ax[1].set_xlim(minimum - 0.05*(maximum-minimum), maximum + 0.05*(maximum-minimum))
minimum = min(data[:, 1])*1e09
maximum = max(data[:, 1])*1e09
ax[1].set_ylim(minimum - 0.05*(maximum-minimum), maximum + 0.05*(maximum-minimum))
ax[1].set(xlabel = r'$I_B$ $[nA]$', ylabel = r'$I_E$ or $I_C$ $[nA]$')

beta = (data[:, 1] - data[:, 3]) / data[:, 3]
ax[2].scatter(data[:, 3]*1e09, beta, marker = '.', label = 'beta')
ax[2].set(xlabel = r'$I_B$ $[nA]$', ylabel = r'$\beta$')

ax[0].legend()
ax[0].grid()
ax[1].legend()
ax[1].grid()
ax[2].legend()
ax[2].grid()

fig.suptitle(data_folder)

plt.tight_layout()
plt.subplots_adjust(top=0.9)
plt.show()

