import numpy as np
import matplotlib.pyplot as plt
import matplotlib.colors as colors
import matplotlib.ticker as ticker
import os
import sys

#root_path = 'C:/Users/lab2/data/SiGe/HBT/Device1/2020-06-03/'
#root_path = '../06_Analysis/2020-06-04/Refined_Data/'
root_path = '../04_Data/2020-06-09/'
root_path = root_path if len(sys.argv) == 1 else sys.argv[1]
data_file_ending = '.dat'
figsize = (12,8)
only_plot_every_nth = 1

def select_data():
      file_list = sorted(os.listdir('{}.'.format(root_path)))
      for i in np.arange(len(file_list))[::-1]:
      	if '.' in file_list[i]:
      		file_list.pop(i)

      for i in range(len(file_list)):
      	print('{}:\t{}'.format(i, file_list[i]))

      user_input = input('select file: ')

      user_input = int((len(file_list) - 1) if (user_input == '') else user_input)

      data_file_name = ''
      for filename in os.listdir('{0}{1}'.format(root_path, file_list[user_input])):
      	if data_file_ending in filename:
      		data_file_name = filename
      		break

      data_folder = file_list[user_input]
      print('selected: {}'.format('{0}{1}/{2}'.format(root_path, data_folder,data_file_name)))
      file = open('{0}{1}/{2}'.format(root_path, data_folder,data_file_name), 'r')
      lines = file.readlines()
      file.close()

      for i in range(len(lines)):
            lines[i] = lines[i].replace('\n','').replace('# ', '').replace('\"', '').replace('nan', '0')

      n_empty_lines = 1
      for i in np.arange(len(lines))[::-1]:
            if lines[i] == '':
                  lines.pop(i)
                  n_empty_lines += 1

      names = lines[0].split('\t')
      data = np.zeros((len(lines) - 3, len(lines[3].split('\t'))))

      for i in range(data.shape[0]):
            for j in range(data.shape[1]):
                  data[i, j] = lines[i+3].split('\t')[j]

      return data, names, data_folder, n_empty_lines

# Analysis
data, names, data_folder, n_empty_lines = select_data()

data[:, 3] = - data[:, 3]

I_B = data[:, 4]
I_E = data[:, 3]
t = data[:, 0]

plt.scatter(t, I_E*1e09 , marker = '.', label = names[3])
plt.scatter(t, I_B*1e09 , marker = '.', label = names[4])

minimum = min([min(I_E), min(I_B)])*1e09
maximum = max([max(I_E), max(I_B)])*1e09
plt.ylim(minimum - 0.05*(maximum-minimum), maximum + 0.05*(maximum-minimum))
plt.xlabel(r'$t$ $[s]$')
plt.ylabel(r'$I$ $[nA]$')

plt.legend()
plt.grid()
plt.title(data_folder)
plt.tight_layout()
plt.show()
#plt.cla()

'''
n, bins, patches = plt.hist(I_B, bins = 30)
plt.cla()
print(bins)
'''

plt.hist(I_B, log = True, bins = 30)

plt.xlabel(r'$I_B$ $[nA]$')
plt.ylabel('#')

plt.grid()
plt.title(data_folder)
plt.tight_layout()
plt.show()

I_temp = []
for I in I_E:
    if I < 11.3480e-09:
        I_temp.append(I)

plt.hist(I_temp, log = False, bins = 50)

plt.xlabel(r'$I_B$ $[nA]$')
plt.ylabel('#')

plt.grid()
plt.title(data_folder)
plt.tight_layout()
plt.show()
