# -*- coding: utf-8 -*-
"""
Created on Mon May 25 15:26:19 2020

@author: Max
"""

import numpy as np
import matplotlib.pyplot as plt
import matplotlib.colors as colors
import matplotlib.ticker as ticker
import os
import sys

#root_path = 'C:/Users/lab2/data/SiGe/HBT/Device1/2020-06-03/'
#root_path = '../06_Analysis/2020-06-04/Refined_Data/'
root_path = '../04_Data/2020-06-09/'
root_path = root_path if len(sys.argv) == 1 else sys.argv[1]
singled = False
singled = singled if len(sys.argv) <= 2 else True
data_file_ending = '.dat'
figsize = (12,8)
only_plot_every_nth = 1

def select_data():
      file_list = sorted(os.listdir('{}.'.format(root_path)))
      for i in np.arange(len(file_list))[::-1]:
      	if '.' in file_list[i]:
      		file_list.pop(i)

      for i in range(len(file_list)):
      	print('{}:\t{}'.format(i, file_list[i]))

      user_input = input('select file: ')

      user_input = int((len(file_list) - 1) if (user_input == '') else user_input)

      data_file_name = ''
      for filename in os.listdir('{0}{1}'.format(root_path, file_list[user_input])):
      	if data_file_ending in filename:
      		data_file_name = filename
      		break

      data_folder = file_list[user_input]
      print('selected: {}'.format('{0}{1}/{2}'.format(root_path, data_folder,data_file_name)))
      file = open('{0}{1}/{2}'.format(root_path, data_folder,data_file_name), 'r')
      lines = file.readlines()
      file.close()

      for i in range(len(lines)):
            lines[i] = lines[i].replace('\n','').replace('# ', '').replace('\"', '').replace('nan', '0')

      n_empty_lines = 1
      for i in np.arange(len(lines))[::-1]:
            if lines[i] == '':
                  lines.pop(i)
                  n_empty_lines += 1

      names = lines[0].split('\t')
      data = np.zeros((len(lines) - 3, len(lines[3].split('\t'))))

      for i in range(data.shape[0]):
            for j in range(data.shape[1]):
                  data[i, j] = lines[i+3].split('\t')[j]

      return data, names, data_folder, n_empty_lines

#%% 2D Sweeps
data, names, data_folder, n_empty_lines = select_data()

temp = []
n_cols = 0
i = 0
while not (data[i, 1] in temp):
      n_cols += 1
      temp.append(data[i, 1])
      i += 1

image0 = np.zeros((n_empty_lines, n_cols))
image1 = np.zeros((n_empty_lines, n_cols))
for i in range(image0.shape[0]):
      for j in range(image0.shape[1]):
            image0[i, j] = -1 * data[i*image0.shape[1] + j, 2]
            image1[i, j] = data[i*image1.shape[1] + j, 4]

# data plotting magic
image0[np.where(image0 > 9e-07)] = 9e-07
image1[np.where(image1 > 9e-07)] = 9e-07

image0[np.where(image0 <= 0)] = 123456
image0[np.where(image0 == 123456)] = np.min(image0)
image1[np.where(image1 <= 0)] = 123456
image1[np.where(image1 == 123456)] = np.min(image1)

data[:, 0] *= 1000 #mV
data[:, 1] *= 1000 #mV

# plotting

def x_formatter_V(x, pos = None):
      return np.round(data[int(x), 1], 3)
def y_formatter_V(y, pos = None):
      y = 0 if y < 0 else y
      y = (n_empty_lines - 1) if y > (n_empty_lines - 1) else y
      return np.round(data[int(y)*n_cols, 0], 3)

fig, ax = plt.subplots(ncols = 2, figsize = figsize, sharey = True)

im0 = ax[0].imshow(image0, aspect = 'auto', norm = colors.LogNorm(vmin = np.min(image0), vmax = np.max(image0)))#, extent = [0, 10, 0, 10])
ax[0].xaxis.set_major_formatter(ticker.FuncFormatter(x_formatter_V))
ax[0].yaxis.set_major_formatter(ticker.FuncFormatter(y_formatter_V))
ax[0].set(xlabel = r'$V_{Base}$ $[mV]$', ylabel = r'$V_{Emitter}$ $[mV]$')
cb0 = fig.colorbar(im0, ax=ax[0], use_gridspec = True)
cb0.set_label(r'$I_E$ $[A]$')

im1 = ax[1].imshow(image1, aspect = 'auto', norm = colors.LogNorm(vmin = np.min(image1), vmax = np.max(image1)))
ax[1].xaxis.set_major_formatter(ticker.FuncFormatter(x_formatter_V))
ax[1].yaxis.set_major_formatter(ticker.FuncFormatter(y_formatter_V))
ax[1].set(xlabel = r'$V_{Base}$ $[mV]$')
cb1 = fig.colorbar(im1, ax=ax[1], use_gridspec = True)
cb1.set_label(r'$I_B$ $[A]$')

fig.suptitle(data_folder)

plt.tight_layout()
plt.subplots_adjust(top=0.9)
plt.show()


