import os
import sys

filename = str(sys.argv[1])
file = open(filename, 'r')
lines = file.readlines()
file.close()

splits = []
split_names = []
split_indices = []
for i in range(len(lines)):
    if '#%%' in lines[i]:
        split_names.append(lines[i].replace('#%% ', '').replace('#%%', '').replace(' ', '_').replace('\n', ''))
        split_indices.append(i)

for i in range(len(split_indices)):
    if i == 0:
        splits.append(lines[:split_indices[i]:])
    else:
        splits.append(lines[split_indices[i-1]:split_indices[i]:])
    if i == len(split_indices) - 1:
        splits.append(lines[split_indices[i]:len(lines):])

for i in range(1, len(splits)):
    file = open(f'{sys.argv[1].replace(".py", "")}_{split_names[i-1]}.py', 'w+')
    for line in splits[0]:
        file.write(line)
    for line in splits[i]:
        file.write(line)
    file.close()