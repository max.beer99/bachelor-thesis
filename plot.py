# -*- coding: utf-8 -*-
"""
Created on Mon May 25 15:26:19 2020

@author: Max
"""

import numpy as np
import matplotlib.pyplot as plt
import matplotlib.colors as colors
import matplotlib.ticker as ticker
import os
import sys

#root_path = 'C:/Users/lab2/data/SiGe/HBT/Device1/2020-06-03/'
#root_path = '../06_Analysis/2020-06-04/Refined_Data/'
root_path = '../04_Data/2020-06-09/'
root_path = root_path if len(sys.argv) == 1 else sys.argv[1]
singled = False
singled = singled if len(sys.argv) <= 2 else True
data_file_ending = '.dat'
figsize = (12,8)
only_plot_every_nth = 1

def select_data():
      file_list = sorted(os.listdir('{}.'.format(root_path)))
      for i in np.arange(len(file_list))[::-1]:
      	if '.' in file_list[i]:
      		file_list.pop(i)

      for i in range(len(file_list)):
      	print('{}:\t{}'.format(i, file_list[i]))

      user_input = input('select file: ')

      user_input = int((len(file_list) - 1) if (user_input == '') else user_input)

      data_file_name = ''
      for filename in os.listdir('{0}{1}'.format(root_path, file_list[user_input])):
      	if data_file_ending in filename:
      		data_file_name = filename
      		break

      data_folder = file_list[user_input]
      print('selected: {}'.format('{0}{1}/{2}'.format(root_path, data_folder,data_file_name)))
      file = open('{0}{1}/{2}'.format(root_path, data_folder,data_file_name), 'r')
      lines = file.readlines()
      file.close()

      for i in range(len(lines)):
            lines[i] = lines[i].replace('\n','').replace('# ', '').replace('\"', '').replace('nan', '0')

      n_empty_lines = 1
      for i in np.arange(len(lines))[::-1]:
            if lines[i] == '':
                  lines.pop(i)
                  n_empty_lines += 1

      names = lines[0].split('\t')
      data = np.zeros((len(lines) - 3, len(lines[3].split('\t'))))

      for i in range(data.shape[0]):
            for j in range(data.shape[1]):
                  data[i, j] = lines[i+3].split('\t')[j]

      return data, names, data_folder, n_empty_lines

#%% 1D Sweeps
data, names, data_folder, n_empty_lines = select_data()

data[:, 0] = - data[:, 0]
data[:, 1] = - data[:, 1]

fig, ax = plt.subplots(ncols = 3, figsize = figsize)

ax[0].scatter(data[:, 0]*1e03, data[:, 1]*1e09 , marker = '.', label = names[1])
ax[0].scatter(data[:, 0]*1e03, data[:, 3]*1e09 , marker = '.', label = names[3])

minimum = min([min(data[:, 1]), min(data[:, 3])])*1e09
maximum = max(data[:, 3])*1e09
ax[0].set_ylim(minimum - 0.05*(maximum-minimum), maximum + 0.05*(maximum-minimum))
ax[0].set(xlabel = r'$V$ $[mV]$', ylabel = r'$I$ $[nA]$')

ax[1].scatter(data[:, 3]*1e09, data[:, 1]*1e09 , marker = '.', label = names[3])

minimum = min(data[:, 3])*1e09
maximum = max(data[:, 3])*1e09
ax[1].set_xlim(minimum - 0.05*(maximum-minimum), maximum + 0.05*(maximum-minimum))
minimum = min(data[:, 1])*1e09
maximum = max(data[:, 1])*1e09
ax[1].set_ylim(minimum - 0.05*(maximum-minimum), maximum + 0.05*(maximum-minimum))
ax[1].set(xlabel = r'$I_B$ $[nA]$', ylabel = r'$I_E$ or $I_C$ $[nA]$')

beta = (data[:, 1] - data[:, 3]) / data[:, 3]
ax[2].scatter(data[:, 3]*1e09, beta, marker = '.', label = 'beta')
ax[2].set(xlabel = r'$I_B$ $[nA]$', ylabel = r'$\beta$')

ax[0].legend()
ax[0].grid()
ax[1].legend()
ax[1].grid()
ax[2].legend()
ax[2].grid()

fig.suptitle(data_folder)

plt.tight_layout()
plt.subplots_adjust(top=0.9)
plt.show()

#%% Time-Res
data, names, data_folder, n_empty_lines = select_data()

data[:, 3] = - data[:, 3]

splits = []
for i in range(1, len(data[:, data.shape[1] - 1])):
    if data[i, data.shape[1] - 1] > data[i-1, data.shape[1] - 1]:
        splits.append(data[i, 0])

if not singled:
    fig, ax = plt.subplots(ncols = 3, figsize = figsize)
    ax[0].scatter(data[:, 0], data[:, 3]*1e09 , marker = '.', label = names[3])
    ax[0].scatter(data[:, 0], data[:, 4]*1e09 , marker = '.', label = names[4])

    minimum = min([min(data[:, 3]), min(data[:, 4])])*1e09
    maximum = max([max(data[:, 3]), max(data[:, 4])])*1e09
    ax[0].vlines(splits, minimum, maximum, alpha = 0.05)
    ax[0].set_ylim(minimum - 0.05*(maximum-minimum), maximum + 0.05*(maximum-minimum))
    ax[0].set(xlabel = r'$t$ $[s]$', ylabel = r'$I$ $[nA]$')

    ax[1].scatter(data[:, 4]*1e09, data[:, 3]*1e09, marker = '.', label = names[3])

    minimum = min(data[:, 4])*1e09
    maximum = max(data[:, 4])*1e09
    ax[1].set_xlim(minimum - 0.05*(maximum-minimum), maximum + 0.05*(maximum-minimum))
    minimum = min(data[:, 3])*1e09
    maximum = max(data[:, 3])*1e09
    ax[1].set_ylim(minimum - 0.05*(maximum-minimum), maximum + 0.05*(maximum-minimum))
    ax[1].set(xlabel = r'$I_B$ $[nA]$', ylabel = r'$I_E$ or $I_C$ $[nA]$')

    beta = (data[:, 3] - data[:, 4]) / data[:, 4]
    ax[2].scatter(data[:, 4]*1e09, beta, marker = '.', label = 'beta')
    ax[2].set(xlabel = r'$I_B$ $[nA]$', ylabel = r'$\beta$')

    ax[0].legend()
    #ax[0].grid()
    ax[1].legend()
    ax[1].grid()
    ax[2].legend()
    ax[2].grid()

    fig.suptitle(data_folder)

    plt.tight_layout()
    plt.subplots_adjust(top=0.9)
    plt.show()
else:
    plt.scatter(data[:, 0], data[:, 3]*1e09 , marker = '.', label = names[3])
    plt.scatter(data[:, 0], data[:, 4]*1e09 , marker = '.', label = names[4])

    minimum = min([min(data[:, 3]), min(data[:, 4])])*1e09
    maximum = max([max(data[:, 3]), max(data[:, 4])])*1e09
    plt.vlines(splits, minimum, maximum, alpha = 0.05)
    plt.ylim(minimum - 0.05*(maximum-minimum), maximum + 0.05*(maximum-minimum))
    plt.xlabel(r'$t$ $[s]$')
    plt.ylabel(r'$I$ $[nA]$')

    plt.legend()
    plt.grid()
    plt.title(data_folder)
    plt.tight_layout()
    plt.show()

    plt.scatter(data[:, 4]*1e09, data[:, 3]*1e09, marker = '.', label = names[3])

    minimum = min(data[:, 4])*1e09
    maximum = max(data[:, 4])*1e09
    plt.xlim(minimum - 0.05*(maximum-minimum), maximum + 0.05*(maximum-minimum))
    minimum = min(data[:, 3])*1e09
    maximum = max(data[:, 3])*1e09
    plt.ylim(minimum - 0.05*(maximum-minimum), maximum + 0.05*(maximum-minimum))
    plt.xlabel(r'$I_B$ $[nA]$')
    plt.ylabel(r'$I_E$ or $I_C$ $[nA]$')

    plt.legend()
    plt.grid()
    plt.title(data_folder)
    plt.tight_layout()
    plt.show()

    beta = (data[:, 3] - data[:, 4]) / data[:, 4]
    plt.scatter(data[:, 4]*1e09, beta, marker = '.', label = 'beta')
    plt.xlabel(r'$I_B$ $[nA]$')
    plt.ylabel(r'$\beta$')

    plt.legend()
    plt.grid()
    plt.title(data_folder)
    plt.tight_layout()
    plt.show()


#%% 2D Sweeps
data, names, data_folder, n_empty_lines = select_data()

temp = []
n_cols = 0
i = 0
while not (data[i, 1] in temp):
      n_cols += 1
      temp.append(data[i, 1])
      i += 1

image0 = np.zeros((n_empty_lines, n_cols))
image1 = np.zeros((n_empty_lines, n_cols))
for i in range(image0.shape[0]):
      for j in range(image0.shape[1]):
            image0[i, j] = -1 * data[i*image0.shape[1] + j, 2]
            image1[i, j] = data[i*image1.shape[1] + j, 4]

# data plotting magic
image0[np.where(image0 > 9e-07)] = 9e-07
image1[np.where(image1 > 9e-07)] = 9e-07

image0[np.where(image0 <= 0)] = 123456
image0[np.where(image0 == 123456)] = np.min(image0)
image1[np.where(image1 <= 0)] = 123456
image1[np.where(image1 == 123456)] = np.min(image1)

data[:, 0] *= 1000 #mV
data[:, 1] *= 1000 #mV

# plotting

def x_formatter_V(x, pos = None):
      return np.round(data[int(x), 1], 3)
def y_formatter_V(y, pos = None):
      y = 0 if y < 0 else y
      y = (n_empty_lines - 1) if y > (n_empty_lines - 1) else y
      return np.round(data[int(y)*n_cols, 0], 3)

fig, ax = plt.subplots(ncols = 2, figsize = figsize, sharey = True)

im0 = ax[0].imshow(image0, aspect = 'auto', norm = colors.LogNorm(vmin = np.min(image0), vmax = np.max(image0)))#, extent = [0, 10, 0, 10])
ax[0].xaxis.set_major_formatter(ticker.FuncFormatter(x_formatter_V))
ax[0].yaxis.set_major_formatter(ticker.FuncFormatter(y_formatter_V))
ax[0].set(xlabel = r'$V_{Base}$ $[mV]$', ylabel = r'$V_{Emitter}$ $[mV]$')
cb0 = fig.colorbar(im0, ax=ax[0], use_gridspec = True)
cb0.set_label(r'$I_E$ $[A]$')

im1 = ax[1].imshow(image1, aspect = 'auto', norm = colors.LogNorm(vmin = np.min(image1), vmax = np.max(image1)))
ax[1].xaxis.set_major_formatter(ticker.FuncFormatter(x_formatter_V))
ax[1].yaxis.set_major_formatter(ticker.FuncFormatter(y_formatter_V))
ax[1].set(xlabel = r'$V_{Base}$ $[mV]$')
cb1 = fig.colorbar(im1, ax=ax[1], use_gridspec = True)
cb1.set_label(r'$I_B$ $[A]$')

fig.suptitle(data_folder)

plt.tight_layout()
plt.subplots_adjust(top=0.9)
plt.show()


#%% Currents And Beta 2D Sweep
# I_E vs I_B and V_E
data, names, data_folder, n_empty_lines = select_data()

data_list = []
for i in range(data.shape[0]):
    data_list.append(data[i, :])

data_lists_ve = []
i = 0
while data_list != []:
    data_lists_ve.append([])
    temp = data_list[0][0]
    for j in range(len(data_list))[::-1]:
        if data_list[j][0] == temp:
            data_lists_ve[i].append(data_list[j])
            data_list.pop(j)
    i += 1

for i in range(len(data_lists_ve)):
    data_lists_ve[i] = np.array(data_lists_ve[i])

data_ve = np.array(data_lists_ve)

# [:, 2] is I_E and [:, 4] is I_B

# change sign
for i in range(data_ve.shape[0]):
    data_ve[i][:, 2] = - data_ve[i][:, 2]

# cut when I_E > 1e-06 or I_B > 100e-09
for i in range(data_ve.shape[0]):
    data_ve[i][np.where(data_ve[i][:, 2] >= 1e-06)] = 1e-06
    data_ve[i][np.where(data_ve[i][:, 4] >= 100e-09)] = 100e-09

'''
# show only specified V_B
for i in range(data_ve.shape[0]):
    data_ve[i][np.where(data_ve[i][:, 1] != 0)] = 0
'''

if not singled:
    fix, ax = plt.subplots(ncols = 2, sharex = True)

    for i in range(data_ve.shape[0]):
          ax[0].scatter(data_ve[i][:, 4]*1e09, data_ve[i][:, 2]*1e09, marker = '.', label = data_ve[i][0, 0])

    flattened_x = []
    flattened_y = []
    for i in range(data_ve.shape[0]):
        flattened_x.append(data_ve[i][:, 4].flatten()*1e09)
        flattened_y.append(data_ve[i][:, 2].flatten()*1e09)
    flattened_x = np.concatenate(flattened_x)
    flattened_y = np.concatenate(flattened_y)
    min_x = np.min(flattened_x)
    max_x = np.max(flattened_x)
    min_y = np.min(flattened_y)
    max_y = np.max(flattened_y)

    ax[0].grid()
    ax[0].set_xlim(min_x - (max_x - min_x) * 0.1, max_x + (max_x - min_x) * 0.1)
    ax[0].set_ylim(min_y - (max_y - min_y) * 0.1, max_y + (max_y - min_y) * 0.1)
    ax[0].legend()

    plt.show()

    #TODO:  Plot Beta
else:
    for i in range(data_ve.shape[0]):
          plt.scatter(data_ve[i][:, 4]*1e09, data_ve[i][:, 2]*1e09, marker = '.', label = data_ve[i][0, 0])

    flattened_x = []
    flattened_y = []
    for i in range(data_ve.shape[0]):
        flattened_x.append(data_ve[i][:, 4].flatten()*1e09)
        flattened_y.append(data_ve[i][:, 2].flatten()*1e09)
    flattened_x = np.concatenate(flattened_x)
    flattened_y = np.concatenate(flattened_y)
    min_x = np.min(flattened_x)
    max_x = np.max(flattened_x)
    min_y = np.min(flattened_y)
    max_y = np.max(flattened_y)

    plt.grid()
    plt.xlim(min_x - (max_x - min_x) * 0.1, max_x + (max_x - min_x) * 0.1)
    plt.ylim(min_y - (max_y - min_y) * 0.1, max_y + (max_y - min_y) * 0.1)
    plt.xlabel(r'$I_B$ $[nA]$')
    plt.ylabel(r'$I_E$ or $I_C$ $[nA]$')
    plt.title(data_folder)

    plt.show()
