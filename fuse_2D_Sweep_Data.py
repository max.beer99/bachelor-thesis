import numpy as np
import os
import sys
import time

index = int(sys.argv[1])
root_path = './output/'
data_file_ending = '.dat'
figsize = (12,8)
only_plot_every_nth = 1
output = f'output/{time.strftime("%Y-%m-%d_%H-%M-%S")}_Combined_2D_Sweep'

def select_lines():
    file_list = os.listdir('{}.'.format(root_path))
    for i in np.arange(len(file_list))[::-1]:
    	if '.' in file_list[i]:
    		file_list.pop(i)

    for i in range(len(file_list)):
    	print('{}:\t{}'.format(i, file_list[i]))

    user_input = input('select file: ')

    user_input = int((len(file_list) - 1) if (user_input == '') else user_input)

    data_file_name = ''
    for filename in os.listdir('{0}{1}'.format(root_path, file_list[user_input])):
    	if data_file_ending in filename:
    		data_file_name = filename
    		break

    data_folder = file_list[user_input]
    print('selected: {}'.format('{0}{1}/{2}'.format(root_path, data_folder,data_file_name)))
    file = open('{0}{1}/{2}'.format(root_path, data_folder,data_file_name), 'r')
    lines = file.readlines()
    file.close()

    return lines, data_folder
    
lines_all = []
data_names = []
for i in range(index):
    lines, data_folder = select_lines()
    lines_all.append(lines)
    data_names.append(data_folder)

prefix = lines_all[0][:3:]
temp = []   
for i in range(len(lines_all)):
    if i > 0:
        temp.append('\n')
    for j in range(len(lines_all[i])):
        if not '#' in lines_all[i][j]:
            temp.append(lines_all[i][j])

lines = temp

for i in range(1, len(lines)):
    assert len(lines[i].replace('\n', '').split('\t')) >= 7 or len(lines[i].replace('\n', '').split('\t')) == 1
    if len(lines[i].replace('\n', '').split('\t')) >= 7:
        if len(lines[i-1].replace('\n', '').split('\t')) >= 7:
            assert lines[i].replace('\n', '').split('\t')[6] >= lines[i-1].replace('\n', '').split('\t')[6]
        else:
            assert lines[i].replace('\n', '').split('\t')[6] >= lines[i-2].replace('\n', '').split('\t')[6]
    
lines = prefix + lines

try:
    os.mkdir(output)
except:
    pass
    
file = open(f'{output}/data.dat', 'w+')
for line in lines:
    file.write(line)
file.close()

file = open(f'{output}/sources.txt', 'w+')
for name in data_names:
    file.write(f'{name}\n')
file.close()


