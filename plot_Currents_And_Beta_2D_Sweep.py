# -*- coding: utf-8 -*-
"""
Created on Mon May 25 15:26:19 2020

@author: Max
"""

import numpy as np
import matplotlib.pyplot as plt
import matplotlib.colors as colors
import matplotlib.ticker as ticker
import os
import sys

#root_path = 'C:/Users/lab2/data/SiGe/HBT/Device1/2020-06-03/'
#root_path = '../06_Analysis/2020-06-04/Refined_Data/'
root_path = '../04_Data/2020-06-09/'
root_path = root_path if len(sys.argv) == 1 else sys.argv[1]
singled = False
singled = singled if len(sys.argv) <= 2 else True
data_file_ending = '.dat'
figsize = (12,8)
only_plot_every_nth = 1

def select_data():
      file_list = sorted(os.listdir('{}.'.format(root_path)))
      for i in np.arange(len(file_list))[::-1]:
      	if '.' in file_list[i]:
      		file_list.pop(i)

      for i in range(len(file_list)):
      	print('{}:\t{}'.format(i, file_list[i]))

      user_input = input('select file: ')

      user_input = int((len(file_list) - 1) if (user_input == '') else user_input)

      data_file_name = ''
      for filename in os.listdir('{0}{1}'.format(root_path, file_list[user_input])):
      	if data_file_ending in filename:
      		data_file_name = filename
      		break

      data_folder = file_list[user_input]
      print('selected: {}'.format('{0}{1}/{2}'.format(root_path, data_folder,data_file_name)))
      file = open('{0}{1}/{2}'.format(root_path, data_folder,data_file_name), 'r')
      lines = file.readlines()
      file.close()

      for i in range(len(lines)):
            lines[i] = lines[i].replace('\n','').replace('# ', '').replace('\"', '').replace('nan', '0')

      n_empty_lines = 1
      for i in np.arange(len(lines))[::-1]:
            if lines[i] == '':
                  lines.pop(i)
                  n_empty_lines += 1

      names = lines[0].split('\t')
      data = np.zeros((len(lines) - 3, len(lines[3].split('\t'))))

      for i in range(data.shape[0]):
            for j in range(data.shape[1]):
                  data[i, j] = lines[i+3].split('\t')[j]

      return data, names, data_folder, n_empty_lines

#%% Currents And Beta 2D Sweep
# I_E vs I_B and V_E
data, names, data_folder, n_empty_lines = select_data()

data_list = []
for i in range(data.shape[0]):
    data_list.append(data[i, :])

data_lists_ve = []
i = 0
while data_list != []:
    data_lists_ve.append([])
    temp = data_list[0][0]
    for j in range(len(data_list))[::-1]:
        if data_list[j][0] == temp:
            data_lists_ve[i].append(data_list[j])
            data_list.pop(j)
    i += 1

for i in range(len(data_lists_ve)):
    data_lists_ve[i] = np.array(data_lists_ve[i])

data_ve = np.array(data_lists_ve)

# [:, 2] is I_E and [:, 4] is I_B

# change sign
for i in range(data_ve.shape[0]):
    data_ve[i][:, 2] = - data_ve[i][:, 2]

# cut when I_E > 1e-06 or I_B > 100e-09
for i in range(data_ve.shape[0]):
    data_ve[i][np.where(data_ve[i][:, 2] >= 1e-06)] = 1e-06
    data_ve[i][np.where(data_ve[i][:, 4] >= 100e-09)] = 100e-09

'''
# show only specified V_B
for i in range(data_ve.shape[0]):
    data_ve[i][np.where(data_ve[i][:, 1] != 0)] = 0
'''

if not singled:
    fix, ax = plt.subplots(ncols = 2, sharex = True)

    for i in range(data_ve.shape[0]):
          ax[0].scatter(data_ve[i][:, 4]*1e09, data_ve[i][:, 2]*1e09, marker = '.', label = data_ve[i][0, 0])

    flattened_x = []
    flattened_y = []
    for i in range(data_ve.shape[0]):
        flattened_x.append(data_ve[i][:, 4].flatten()*1e09)
        flattened_y.append(data_ve[i][:, 2].flatten()*1e09)
    flattened_x = np.concatenate(flattened_x)
    flattened_y = np.concatenate(flattened_y)
    min_x = np.min(flattened_x)
    max_x = np.max(flattened_x)
    min_y = np.min(flattened_y)
    max_y = np.max(flattened_y)

    ax[0].grid()
    ax[0].set_xlim(min_x - (max_x - min_x) * 0.1, max_x + (max_x - min_x) * 0.1)
    ax[0].set_ylim(min_y - (max_y - min_y) * 0.1, max_y + (max_y - min_y) * 0.1)
    ax[0].legend()

    plt.show()

    #TODO:  Plot Beta
else:
    for i in range(data_ve.shape[0]):
          plt.scatter(data_ve[i][:, 4]*1e09, data_ve[i][:, 2]*1e09, marker = '.', label = data_ve[i][0, 0])

    flattened_x = []
    flattened_y = []
    for i in range(data_ve.shape[0]):
        flattened_x.append(data_ve[i][:, 4].flatten()*1e09)
        flattened_y.append(data_ve[i][:, 2].flatten()*1e09)
    flattened_x = np.concatenate(flattened_x)
    flattened_y = np.concatenate(flattened_y)
    min_x = np.min(flattened_x)
    max_x = np.max(flattened_x)
    min_y = np.min(flattened_y)
    max_y = np.max(flattened_y)

    plt.grid()
    plt.xlim(min_x - (max_x - min_x) * 0.1, max_x + (max_x - min_x) * 0.1)
    plt.ylim(min_y - (max_y - min_y) * 0.1, max_y + (max_y - min_y) * 0.1)
    plt.xlabel(r'$I_B$ $[nA]$')
    plt.ylabel(r'$I_E$ or $I_C$ $[nA]$')
    plt.title(data_folder)

    plt.show()
