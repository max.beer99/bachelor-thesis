import numpy as np
import os
import sys

root_path = '../04_Data/2020-06-08_Long_Scan/'
data_file_ending = '.dat'
figsize = (12,8)
only_plot_every_nth = 1
output = 'output/{data_folder}/'

def select_lines():
    file_list = os.listdir('{}.'.format(root_path))
    for i in np.arange(len(file_list))[::-1]:
    	if '.' in file_list[i]:
    		file_list.pop(i)

    for i in range(len(file_list)):
    	print('{}:\t{}'.format(i, file_list[i]))

    file_data_list = []
    data_folder_list = []
    for entry in file_list:
        data_file_name = ''
        for filename in os.listdir('{0}{1}'.format(root_path, entry)):
            if data_file_ending in filename:
                data_file_name = filename
                break

        data_folder = entry
        print('selected: {}'.format('{0}{1}/{2}'.format(root_path, data_folder,data_file_name)))
        file = open('{0}{1}/{2}'.format(root_path, data_folder,data_file_name), 'r')
        file_data_list.append(file.readlines())
        file.close()
        data_folder_list.append(data_folder)

    return file_data_list, data_folder_list
    
lines_list, data_folder_list = select_lines()

for lines_index in range(len(lines_list)):
    for i in range(3, len(lines_list[lines_index])):
        if not lines_list[lines_index][i] == '\n':
            lines_list[lines_index][i] = lines_list[lines_index][i].replace('\n', f'\t{lines_index}\n')
        
    try:
        os.mkdir(f'output/{data_folder_list[lines_index]}')
    except:
        pass
        
    file = open(f'{output.format(data_folder = data_folder_list[lines_index])}data.dat', 'w+')
    for line in lines_list[lines_index]:
        file.write(line)
    file.close()