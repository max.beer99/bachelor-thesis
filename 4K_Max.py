# -*- coding: utf-8 -*-
"""
Created on Mon May 25 10:57:35 2020

@author: Max Beer
"""

#%%
import time
import qcodes as qc
from qcodes.instrument_drivers.Harvard.FZJ_Decadac import Decadac
from qcodes.instrument_drivers.stanford_research.SR830 import SR830
from qcodes.instrument_drivers.tektronix.Keithley_2450 import Keithley2450
import qcodes.instrument_drivers.Lakeshore.Model_325 as ls #Temperature sensor
import os
#Magnet Power Supply missing here
from qcodes.tests.instrument_mocks import DummyInstrument

from qcodes.plots.pyqtgraph import QtPlot
from qcodes.instrument.parameter import ManualParameter
from qcodes.instrument.parameter import Parameter
from qcodes.utils.validators import Numbers

from qcodes.plots.qcmatplotlib import MatPlot

import numpy as np
import matplotlib.pyplot as plt
from tqdm import tqdm

#%% Load all instruments
qc.Instrument.close_all()
    

#keithley1=Keithley2450('keithley1', 'GPIB1::23::INSTR')
keithley2=Keithley2450('keithley2', 'GPIB1::18::INSTR')
keithley1 = keithley2
lockin = keithley2

#lockin=SR830("lockin",'GPIB1::12::INSTR')

#counter=DummyInstrument(name="dummy2")
#counter.add_parameter('count', set_cmd=None)

t0=time.time()
def experiment_time(t0=t0):
      t= round((time.time()-t0),4)
      return t

#%% Add instruments to the station
station=qc.Station(keithley1,keithley2, lockin)
print('station is now set up with the following instruments:\n %s',
      station.components)

#%% Set location here

#I recommend to add a scheme for the folder structure as a comment here asap
location='C:/Users/lab2/data/SiGe/HBT/Device1/{date}/{date}_{time}_{name}'
loc_provider = qc.data.location.FormatLocation(fmt=location)
qc.data.data_set.DataSet.location_provider=loc_provider

#%%
def ramp_keithley(value, step=0.1, wait=0.01, device=keithley1):
      
      if not device.output_enabled.get():
            print("Device output is off. Please enable device output first")
            return False
      if not device.source_function.get()=='voltage':
            print("Ramping only supported for devices in voltage-source mode. Please set source_funtion to voltage")
            return False
      
      try:  
            if device.source.voltage.get()==value:
                  print("Target value already reached")
                  return True
            assert step > 0
            if device.source.voltage.get()>value:
                  step*=-1
                  
            
            set_value=device.source.voltage.get()
            while set_value<value-np.abs(step) or set_value>value+np.abs(step):
                  set_value+=step
                  device.source.voltage.set(set_value)
           
                  time.sleep(wait)

            device.source.voltage.set(value)
            
            return True
      except:
            print("Something went wrong")
            
def sweep_and_sense(start, stop, step, wait=0.1, device1 = keithley1, device2 = keithley2, name = '1D', t0 = 0):
      t0 = time.time() if t0 == 0 else t0
      current_loc = location.format(date = time.strftime('%Y-%m-%d'), time = time.strftime('%H-%M-%S'), name = name)
      os.makedirs(current_loc)
      current_loc = '{}{}'.format(current_loc, '/data.dat')
      file = open(current_loc, 'w+')
      file.write('# {}\t{}\t{}\t{}\t{}\t{}\n'.format('U_1', 'I_1', 'U_1', 'I_2', 'U_2', 't')) 
      file.write('# null\n# null\n')
      
      if not device1.output_enabled.get():
            print("Device output is off. Please enable device output first")
            return False
      if not device1.source_function.get()=='voltage' or not device2.source_function.get()=='voltage':
            print("Function only supported for devices in voltage-source mode. Please set source_funtion to voltage")
            return False
      if not device1.sense_function.get()=='current' or not device2.sense_function.get()=='current':
            print("Function only supported for devices in current-sense mode. Please set sense_funtion to current")
            return False
      
      if start > stop:
            step = -1 * np.abs(step)
      
      try:
            current_value = device1.source.voltage.get()
            if current_value != start:
                  ramp_keithley(start, step = np.abs(start - current_value)/20 , device = device1)
                  time.sleep(5)
            device1.source.voltage.set(start)
            time.sleep(wait * 4)
            
            current_value = device1.source.voltage.get()
            while current_value < (stop - np.abs(step)/2) or current_value > (stop + np.abs(step)/2):
                  new_value = current_value + step
                  device1.source.voltage.set(new_value)
                  
                  time.sleep(wait)
                  current_time = experiment_time(t0=t0)
                  readout_V1 = device1.source.voltage.get()
                  readout_V2 = device2.source.voltage.get()
                  readout_I1 = device1.sense.current.get()
                  readout_I2 = device2.sense.current.get()
                  toWrite = '{}\t{}\t{}\t{}\t{}\t{}\n'.format(readout_V1, readout_I1, readout_V1, readout_I2, readout_V2, current_time)
                  file.write(toWrite)
                  time.sleep(wait)
                  
                  current_value = new_value
     
            file.close()
      except:
            file.close()
            print("Something went wrong")
      
      print('\nsaved to: {}'.format(current_loc))
      
def sweep_and_sense_lockin(start, stop, step, wait=0.1, device1 = keithley1, lockin = lockin, lockin_amp = 0, lockin_freq = 70, name = '1D_Lockin', t0 = 0):
      t0 = time.time() if t0 == 0 else t0
      current_loc = location.format(date = time.strftime('%Y-%m-%d'), time = time.strftime('%H-%M-%S'), name = name)
      os.makedirs(current_loc)
      current_loc = '{}{}'.format(current_loc, '/data.dat')
      file = open(current_loc, 'w+')
      file.write('# {}\t{}\t{}\t{}\t{}\t{}\n'.format('U_1', 'I_1', 'U_1', 'I_2', 'U_2', 't')) 
      file.write('# null\n# null\n')
      
      if not device1.output_enabled.get():
            print("Device output is off. Please enable device output first")
            return False
      if not device1.source_function.get()=='voltage':
            print("Function only supported for devices in voltage-source mode. Please set source_funtion to voltage")
            return False
      if not device1.sense_function.get()=='current':
            print("Function only supported for devices in current-sense mode. Please set sense_funtion to current")
            return False
      
      if start > stop:
            step = -1 * np.abs(step)
      
      try:
            lockin.frequency.set(lockin_freq)
            lockin.amplitude.set(lockin_amp)
            time.sleep(1)
            current_value = device1.source.voltage.get()
            if current_value != start:
                  ramp_keithley(start, step = np.abs(start - current_value)/20 , device = device1)
                  time.sleep(5)
            device1.source.voltage.set(start)
            time.sleep(wait * 4)
            
            current_value = device1.source.voltage.get()
            while current_value < (stop - np.abs(step)/2) or current_value > (stop + np.abs(step)/2):
                  new_value = current_value + step
                  device1.source.voltage.set(new_value)
                  
                  time.sleep(wait)
                  current_time = experiment_time(t0=t0)
                  readout_V1 = device1.source.voltage.get()
                  readout_V2_Out = lockin.amplitude.get()
                  readout_I1 = device1.sense.current.get()
                  readout_V2_In = lockin.R()
                  toWrite = '{}\t{}\t{}\t{}\t{}\t{}\n'.format(readout_V1, readout_I1, readout_V1, readout_V2_In, readout_V2_Out, current_time)
                  file.write(toWrite)
                  time.sleep(wait)
                  
                  current_value = new_value
     
            file.close()
      except:
            file.close()
            print("Something went wrong")
      
      print('\nsaved to: {}'.format(current_loc))
            
def sweep_and_sense_2d(start_1, stop_1, step_1, start_2, stop_2, step_2, wait=0.1, device1 = keithley1, device2 = keithley2, name = '2D', t0 = 0):  
      t0 = time.time() if t0 == 0 else t0
      current_loc = location.format(date = time.strftime('%Y-%m-%d'), time = time.strftime('%H-%M-%S'), name = name)
      os.makedirs(current_loc)
      current_loc = '{}{}'.format(current_loc, '/data.dat')
      file = open(current_loc, 'w+')
      file.write('# {}\t{}\t{}\t{}\t{}\t{}\t{}\n'.format('U_1', 'U_2', 'I_1', 'U_1', 'I_2', 'U_2', 't')) 
      file.write('# null\n# null')
      
      if not device1.output_enabled.get():
            print("Device output is off. Please enable device output first")
            return False
      if not device1.source_function.get()=='voltage' or not device2.source_function.get()=='voltage':
            print("Function only supported for devices in voltage-source mode. Please set source_funtion to voltage")
            return False
      if not device1.sense_function.get()=='current' or not device2.sense_function.get()=='current':
            print("Function only supported for devices in current-sense mode. Please set sense_funtion to current")
            return False
      
      if start_1 > stop_1:
            step_1 = -1 * np.abs(step_1)
      if start_2 > stop_2:
            step_2 = -1 * np.abs(step_2)
            
      stop_1 += step_1
      stop_2 += step_2
      
      try:
            current_voltage_1 = device1.source.voltage.get()
            current_voltage_2 = device2.source.voltage.get()
            if current_voltage_1 != start_1:
                  ramp_keithley(start_1, step = np.abs(start_1 - current_voltage_1)/20 , device = device1)
            if current_voltage_2 != start_2:
                  ramp_keithley(start_2, step = np.abs(start_2 - current_voltage_2)/20 , device = device2)
            time.sleep(10)
            
            device1.source.voltage.set(start_1)
            device2.source.voltage.set(start_2)
            time.sleep(wait * 4)
            
            current_voltage_1 = device1.source.voltage.get()
            current_voltage_2 = device2.source.voltage.get()
            while current_voltage_1 < (stop_1 - np.abs(step_1)/2) or current_voltage_1 > (stop_1 + np.abs(step_1)/2):
                  file.write('\n')
                  new_voltage_1 = current_voltage_1
                  device1.source.voltage.set(new_voltage_1)
                  
                  while current_voltage_2 < (stop_2 - np.abs(step_2)/2) or current_voltage_2 > (stop_2 + np.abs(step_2)/2): 
                        new_voltage_2 = current_voltage_2
                        device2.source.voltage.set(new_voltage_2)
                  
                        time.sleep(wait)
                        current_time = experiment_time(t0=t0)
                        readout_V1 = device1.source.voltage.get()
                        readout_V2 = device2.source.voltage.get()
                        readout_I1 = device1.sense.current.get()
                        readout_I2 = device2.sense.current.get()
                        toWrite = '{}\t{}\t{}\t{}\t{}\t{}\t{}\n'.format(readout_V1, readout_V2, readout_I1, readout_V1, readout_I2, readout_V2, current_time)
                        file.write(toWrite)
                        time.sleep(wait)
                  
                        current_voltage_2 = new_voltage_2 + step_2
                  current_voltage_2 = start_2
                  device2.source.voltage.set(current_voltage_2)
                  current_voltage_1 = new_voltage_1 + step_1
                  print(np.round(100 * (current_voltage_1 - start_1) / (stop_1 - start_1)))
                  
            file.close()
      except:
            file.close()
            print("Something went wrong")
      
      print('\nsaved to: {}'.format(current_loc))
            
def set_and_sense(voltage_1, voltage_2, delta_t, n_measurements, device1 = keithley1, device2 = keithley2, t0 = 0, name = 'Time_Res', index = 0):
      t0 = time.time() if t0 == 0 else t0
      current_voltage_1 = device1.source.voltage.get()
      current_voltage_2 = device2.source.voltage.get()
      if current_voltage_1 != voltage_1:
            ramp_keithley(voltage_1, step = np.abs(voltage_1 - current_voltage_1)/10 , device = device1)
      if current_voltage_2 != voltage_2:
            ramp_keithley(voltage_2, step = np.abs(voltage_2 - current_voltage_2)/10 , device = device2)
      time.sleep(10)
      
      current_loc = location.format(date = time.strftime('%Y-%m-%d'), time = time.strftime('%H-%M-%S'), name = name)
      os.makedirs(current_loc)
      current_loc = '{}{}'.format(current_loc, '/data.dat')
      file = open(current_loc, 'w+')
      file.write('# {}\t{}\t{}\t{}\t{}\n'.format('t', 'U_1', 'U_2', 'I_1', 'I_2'))
      file.write('# null\n# null\n')
      
      device1.source.voltage.set(voltage_1)
      device2.source.voltage.set(voltage_2)
      time.sleep(delta_t)
      
      try:
            for i in tqdm(range(n_measurements)):
                  current_time = experiment_time(t0=t0)
                  readout_V1 = device1.source.voltage.get()
                  readout_V2 = device2.source.voltage.get()
                  readout_I1 = device1.sense.current.get()
                  readout_I2 = device2.sense.current.get()
                  toWrite = '{}\t{}\t{}\t{}\t{}\t{}\n'.format(current_time, readout_V1, readout_V2, readout_I1, readout_I2, index)
                  file.write(toWrite)
                  time.sleep(delta_t)
      
            file.close()            
      except:
            file.close()
      
      print('\nsaved to: {}'.format(current_loc))

      
#%% Define Values
emitter_device = keithley1
basis_device = keithley2
basis_channel = basis_device.source.voltage
emitter_channel = emitter_device.source.voltage
addOn = ''

# Emitter Params
V_Emitter_Start = -890e-03
V_Emitter_Ziel = -955e-03
npoints_Emitter = 150

# Basis Params
V_Basis_Start = 0e-03
V_Basis_Ziel = 0e-03
npoints_Basis = 1

delay_time = 1

lockin_freq = 74.25
lockin_amp = 0.5

sweep_Emitter_range = [V_Emitter_Start, V_Emitter_Ziel]
sweep_Emitter_step = (sweep_Emitter_range[1] - sweep_Emitter_range[0]) / npoints_Emitter
sweep_Basis_range = [V_Basis_Start, V_Basis_Ziel]
sweep_Basis_step = (sweep_Basis_range[1] - sweep_Basis_range[0]) / npoints_Basis

#%% Initialize
current_Emitter_Voltage = emitter_channel.get()
current_Basis_Voltage = basis_channel.get()

# savely initiate to starting voltages
print('initializing...')
time.sleep(1)
print('ramping up V_Emitter...')
ramp_keithley(V_Emitter_Start, np.abs(current_Emitter_Voltage - V_Emitter_Start)/20, delay_time, emitter_device)
print('ramping up V_Basis...')
ramp_keithley(V_Basis_Start, np.abs(current_Basis_Voltage - V_Basis_Start)/20, delay_time, basis_device)

for i in tqdm(range(100)):
      time.sleep(0.1)

#%%

#%% sweep Emitter
emitter_channel.set(V_Emitter_Start)
time.sleep(2)
sweep_Emitter = qc.Loop(emitter_channel.sweep(sweep_Emitter_range[0],sweep_Emitter_range[1],sweep_Emitter_step), delay=delay_time).each(emitter_device.sense.current, emitter_channel, basis_channel, basis_device.sense.current)
data_Emitter = sweep_Emitter.get_data_set(name="Emitter_Sweep" + addOn)
plot = qc.QtPlot()
plot.add(data_Emitter.keithley1_sense_current, subplot=1)
plot.add(data_Emitter.keithley2_sense_current, subplot=1)
sweep_Emitter.with_bg_task(plot.update).run()

#%% sweep Basis
basis_channel.set(V_Basis_Start)
time.sleep(2)
sweep_Basis = qc.Loop(basis_channel.sweep(sweep_Basis_range[0],sweep_Basis_range[1],sweep_Basis_step), delay=delay_time).each(emitter_device.sense.current, emitter_channel, basis_channel, basis_device.sense.current)
data_Basis = sweep_Basis.get_data_set(name="Basis_Sweep" + addOn)
plot = qc.QtPlot()
plot.add(data_Basis.keithley1_sense_current, subplot=1)
plot.add(data_Basis.keithley2_sense_current, subplot=1)
sweep_Basis.with_bg_task(plot.update).run()

#%% sweep 2D
loop_2d = qc.Loop(emitter_channel.sweep(sweep_Emitter_range[0],sweep_Emitter_range[1],sweep_Emitter_step), delay=2*delay_time).loop(basis_channel.sweep(sweep_Basis_range[0],sweep_Basis_range[1], sweep_Basis_step), delay=delay_time).each(emitter_device.sense.current, emitter_channel, basis_channel, basis_device.sense.current)
data_2d = loop_2d.get_data_set(name="2D_Sweep" + addOn)
plot= qc.QtPlot()
plot.add(data_2d.keithley1_sense_current, subplot=1)
plot.add(data_2d.keithley2_sense_current, subplot=2)
loop_2d.with_bg_task(plot.update).run()

#%% sweep Emitter new
sweep_and_sense(start = sweep_Emitter_range[0], stop = sweep_Emitter_range[1], step = sweep_Emitter_step, wait = delay_time, device1 = emitter_device, device2 = basis_device, name = 'Sweep_Emitter') 

#%% sweep Emitter lockin
sweep_and_sense_lockin(start = sweep_Emitter_range[0], stop = sweep_Emitter_range[1], step = sweep_Emitter_step, wait = delay_time, device1 = emitter_device, lockin = lockin, lockin_freq = lockin_freq, lockin_amp = lockin_amp, name = 'Sweep_Emitter') 

#%% sweep 2D new
name_2d = '2D_Sweep'
sweep_and_sense_2d(sweep_Emitter_range[0], sweep_Emitter_range[1], sweep_Emitter_step, sweep_Basis_range[0], sweep_Basis_range[1], sweep_Basis_step, wait = delay_time, device1 = emitter_device, device2 = basis_device, name = name_2d)

#%% Time-Res static
set_and_sense(voltage_1 = -916.75e-03, voltage_2 = 3.66667e-03, delta_t = 1, n_measurements = 7000)

#%% Time-Res moving sweep
for u_e in np.linspace(-890e-03, -955e-03, 10):
      for u_b in np.arange(0, 10.5e-03, 0.5e-03):
            print(np.round(100*np.abs(u_e + 890e-03)/np.abs(890e-03-955e-03), 4), np.round(100*u_b/10e-03, 4))
            set_and_sense(voltage_1 = u_e, voltage_2 = u_b, delta_t = 1, n_measurements = 180)
            
current_Emitter_Voltage = emitter_channel.get()
current_Basis_Voltage = basis_channel.get()

print('Ramping down...')
time.sleep(1)
print('ramping down V_Emitter...')
ramp_keithley(0, np.abs(current_Emitter_Voltage)/20, delay_time, emitter_device)
print('ramping down V_Basis...')
ramp_keithley(0, np.abs(current_Basis_Voltage)/20, delay_time, basis_device)

for i in tqdm(range(100)):
      time.sleep(0.1)

#%% Time-Res moving

set_and_sense(voltage_1 = -917e-03, voltage_2 = 8.1e-03, delta_t = 0.6, n_measurements = 250)
set_and_sense(voltage_1 = -917e-03, voltage_2 = 8.2e-03, delta_t = 0.6, n_measurements = 250)
set_and_sense(voltage_1 = -917e-03, voltage_2 = 8.3e-03, delta_t = 0.6, n_measurements = 250)
set_and_sense(voltage_1 = -917e-03, voltage_2 = 8.4e-03, delta_t = 0.6, n_measurements = 250)
set_and_sense(voltage_1 = -917e-03, voltage_2 = 8.5e-03, delta_t = 0.6, n_measurements = 250)
set_and_sense(voltage_1 = -917e-03, voltage_2 = 8.6e-03, delta_t = 0.6, n_measurements = 250)

#%%
current_Emitter_Voltage = emitter_channel.get()
current_Basis_Voltage = basis_channel.get()

print('Ramping down...')
time.sleep(1)
print('ramping down V_Emitter...')
ramp_keithley(0, np.abs(current_Emitter_Voltage)/20, delay_time, emitter_device)
print('ramping down V_Basis...')
ramp_keithley(0, np.abs(current_Basis_Voltage)/20, delay_time, basis_device)

for i in tqdm(range(100)):
      time.sleep(0.1)
