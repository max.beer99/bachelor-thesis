import numpy as np
import os
import sys

index = int(sys.argv[1])
root_path = '../04_Data/2020-06-03/'
data_file_ending = '.dat'
figsize = (12,8)
only_plot_every_nth = 1
output = 'output/{data_folder}/'

def select_lines():
    file_list = os.listdir('{}.'.format(root_path))
    for i in np.arange(len(file_list))[::-1]:
    	if '.' in file_list[i]:
    		file_list.pop(i)

    for i in range(len(file_list)):
    	print('{}:\t{}'.format(i, file_list[i]))

    user_input = input('select file: ')

    user_input = int((len(file_list) - 1) if (user_input == '') else user_input)

    data_file_name = ''
    for filename in os.listdir('{0}{1}'.format(root_path, file_list[user_input])):
    	if data_file_ending in filename:
    		data_file_name = filename
    		break

    data_folder = file_list[user_input]
    print('selected: {}'.format('{0}{1}/{2}'.format(root_path, data_folder,data_file_name)))
    file = open('{0}{1}/{2}'.format(root_path, data_folder,data_file_name), 'r')
    lines = file.readlines()
    file.close()

    return lines, data_folder
    
lines, data_folder = select_lines()

for i in range(3, len(lines)):
    if not lines[i] == '\n':
        lines[i] = lines[i].replace('\n', f'\t{index}\n')
    
try:
    os.mkdir(f'output/{data_folder}')
except:
    pass
    
file = open(f'{output.format(data_folder = data_folder)}data.dat', 'w+')
for line in lines:
    file.write(line)
file.close()